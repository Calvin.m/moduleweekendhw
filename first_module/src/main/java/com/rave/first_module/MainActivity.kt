package com.rave.first_module

import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.fragment.app.FragmentActivity
import com.rave.first_module.ui.theme.ModulesOnModulesHWTheme

class MainActivity : FragmentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.e("Main Activity: ", "onCreate()")
        setContentView(R.layout.activity_main)
    }

//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        Log.e("Main Activity: ", "onCreate()")
//        setContent {
//            ModulesOnModulesHWTheme {
//                // A surface container using the 'background' color from the theme
//                Surface(
//                    modifier = Modifier.fillMaxSize(),
//                    color = MaterialTheme.colorScheme.background
//                ) {
//                    Text("Hello")
//                }
//            }
//        }
//    }

    override fun onStart() {
        super.onStart()
        Log.e("Main Activity: ", "onStart()")
    }

    override fun onResume() {
        super.onResume()
        Log.e("Main Activity: ", "onResume()")
    }

    override fun onPause() {
        super.onPause()
        Log.e("Main Activity: ", "onPause()")
    }

    override fun onStop() {
        super.onStop()
        Log.e("Main Activity: ", "onStop()")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.e("Main Activity: ", "onDestroy()")
    }
}

package com.rave.fragmentnavigationapp

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.*
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.navigation.findNavController
import com.rave.fragmentnavigationapp.ui.theme.FragmentNavigationAppTheme

class FirstFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
//        return inflater.inflate(R.layout.fragment_first, container, false)

        return ComposeView(requireContext()).apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)

            setContent {
                FragmentNavigationAppTheme() {
                    Surface(
                        color = Color(94, 193, 238, 255)
                    ) {
                        Column() {
                            Row() {
                                Text(
                                    text = "First Fragment",
                                    textAlign = TextAlign.Center,
                                    modifier = Modifier.fillMaxWidth(),
                                    fontWeight = FontWeight.Bold
                                )
                            }
                            Box(modifier = Modifier.fillMaxSize()) {
                                Button(onClick = { findNavController().navigate(R.id.fragment_second) }, modifier = Modifier.align(
                                    Alignment.BottomEnd)) {
                                    Text(text = "Navigate to 2nd Fragment")
                                }
                            }
                        }
                    }


                    // To navigate forward
                    // Activity to Activity -> Intent(this, ActivityToGoToo::class.java)
                    // Fragment to Fragment (with Navigation Component) -> findNavController().navigate(R.id.destinationId)

                    // to go back
                    // Activity -> finish()
                    // Fragment (with Navigation Component) findNavController().navigateUp()

                }
            }
        }
    }

}
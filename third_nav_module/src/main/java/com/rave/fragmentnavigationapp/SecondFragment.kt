package com.rave.fragmentnavigationapp

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.*
import androidx.compose.material3.Button
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.navigation.findNavController
import com.rave.fragmentnavigationapp.ui.theme.FragmentNavigationAppTheme

class SecondFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        return ComposeView(requireContext()).apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)

            setContent {
                FragmentNavigationAppTheme() {
                    Surface(
                        color = Color(63, 169, 218, 255)
                    ) {
                        Column() {
                            Row() {
                                Text(
                                    text = "Second Fragment",
                                    textAlign = TextAlign.Center,
                                    modifier = Modifier.fillMaxWidth(),
                                    fontWeight = FontWeight.Bold
                                )
                            }
                            Box(modifier = Modifier.fillMaxSize()) {
                                Button(
                                    onClick = { findNavController().navigateUp() },
                                    modifier = Modifier.align(
                                        Alignment.BottomStart
                                    )
                                ) {
                                    Text(text = "Back to 1st Fragment")
                                }
                                Button(
                                    onClick = { findNavController().navigate(R.id.fragment_third) },
                                    modifier = Modifier.align(Alignment.BottomEnd)
                                ) {
                                    Text(text = "To 3rd Fragment")
                                }
                            }
                        }


                    }


                }
            }
        }
    }
}
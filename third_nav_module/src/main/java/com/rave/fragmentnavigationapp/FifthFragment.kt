package com.rave.fragmentnavigationapp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.*
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.rave.fragmentnavigationapp.ui.theme.FragmentNavigationAppTheme

class FifthFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        return ComposeView(requireContext()).apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)

            setContent {
                FragmentNavigationAppTheme() {
                    Surface(
                        color = Color(8, 95, 134, 255)
                    ) {
                        Column() {
                            Row() {
                                Text(
                                    text = "Fifth Fragment",
                                    textAlign = TextAlign.Center,
                                    modifier = Modifier.fillMaxWidth(),
                                    fontWeight = FontWeight.Bold
                                )
                            }
                            Box(Modifier.fillMaxSize()) {
                                Button(
                                    onClick = { findNavController().navigate(R.id.fragment_first) },
                                    modifier = Modifier.align(
                                        Alignment.Center
                                    )
                                ) {
                                    Text(text = "<<< Return back to 1st Fragment")
                                }
                                Button(
                                    onClick = { findNavController().navigate(R.id.fragment_fourth) },
                                    modifier = Modifier.align(
                                        Alignment.BottomCenter
                                    )
                                ) {
                                    Text(text = "< Back to 4th Fragment")
                                }

                            }
                        }
                    }
                }
            }
        }
    }
}